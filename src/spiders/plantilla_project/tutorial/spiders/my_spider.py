import scrapy


class MySpider(scrapy.Spider):
    name = "my_spider"
    allowed_domains = ["example.com"]

    def __init__(self, url=None, *args, **kwargs):
        super(MySpider, self).__init__(*args, **kwargs)
        if url:
            self.url = url
        else:
            raise ValueError("Missing required argument: url")

    def start_requests(self):
        url = self.url
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # Aquí implementamos la lógica de parsing
        resultado = "finalizo correctamente"
        pass
