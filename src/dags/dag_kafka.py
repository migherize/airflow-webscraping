""""
Dags para generar mensaje kafka para Airflow.
author: Miguel Herize
mail: migherize@gmail.com
"""
# import os
# from datetime import datetime, timedelta
from airflow.decorators import dag, task
from airflow.utils.dates import days_ago

# pylint: disable=import-error
from kafka import KafkaProducer

import src.utils.constants as constants


def send_message_to_kafka(message):
    """
    funcion para crear el productor de mensaje kafka
    """
    producer = KafkaProducer(
        bootstrap_servers=[constants.KAFKA_BOOTSTRAP_SERVERS],
        value_serializer=lambda x: x.encode("utf-8"),
    )
    producer.send(constants.KAFKA_TOPIC, value=message)
    producer.flush()
    producer.close()


@task
def send_kafka_message_task():
    """
    Tarea para construir y enviar el mensaje para kafka
    """
    message = "Hello, world!"
    print(message)
    send_message_to_kafka(message)


default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": days_ago(1),
}


@dag(default_args=default_args, schedule_interval=None, catchup=False)
def send_kafka_message_dag():
    """
    Dag para el envio del mensaje de kafka.
    """
    send_kafka_message_task()


SEND_KAFKA_MESSAGE_DAG = send_kafka_message_dag()
