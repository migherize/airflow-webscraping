""""
Dags para generar por plantilla Dags scrapy para Airflow.
author: Miguel Herize
mail: migherize@gmail.com
"""
import os
from datetime import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.operators.python import BranchPythonOperator
from airflow.models import Variable
import src.utils.constants as constants

dag = DAG(
    "scrapy_tutorial",
    description="Un DAG que realiza web scraping y guarda los resultados en diferentes ubicaciones",
    schedule_interval="@daily",
    start_date=datetime.now(),
    catchup=False,
)


def _choose():
    resultado_file = Variable.get("resultado_file")
    with open(resultado_file, "r", encoding="utf-8") as file:
        resultado = file.read().strip()

    if resultado == "0":
        return "write_file_success"

    return "write_file_failed"


def guardar_datos_exitosos(**context):
    datos = context["ti"].xcom_pull(task_ids="run_scrapy")
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    info = f"my_spider funciono correctamente a la hora {now}"
    with open(
        os.path.join(constants.AIRFLOW_HOME, constants.REPORTS, constants.SUCCESS),
        "a",
        encoding="utf-8",
    ) as f:
        f.write(str(datos) + info + "\n")


def guardar_datos_fallidos(context):
    datos = context["exception"]
    with open(
        os.path.join(constants.AIRFLOW_HOME, constants.REPORTS, constants.FAILED),
        "a",
        encoding="utf-8",
    ) as f:
        f.write(str(datos) + "\n")


scraping_task = BashOperator(
    task_id="run_scrapy",
    bash_command="/call_script/plantilla_general.sh",
    params={
        "path_script": os.path.join(
            constants.AIRFLOW_HOME,
            constants.PATH_PROJECT_SCRAPY,
            "plantilla_project/tutorial",
        ),
        "scrapy": os.path.join(constants.VIRTUAL_ENV, "bin/scrapy"),
        "spider": "my_spider",
        "url": "https://gitlab.com/migherize/airflow-webscraping",
        "path_reports": os.path.join(constants.AIRFLOW_HOME, constants.REPORTS),
    },
    output_encoding="utf-8",
    env={
        "path_script": "{{ params.path_script }}",
        "scrapy": "{{ params.scrapy }}",
        "script": "{{ params.spider }}",
        "fuente": "{{ params.url }}",
        "path_reports": "{{ params.path_reports }}",
    },
    dag=dag,
)

branchig = BranchPythonOperator(
    task_id="branchPython",
    python_callable=_choose,
)

write_file_success = PythonOperator(
    task_id="write_file_success", python_callable=guardar_datos_exitosos
)

write_file_failed = PythonOperator(
    task_id="write_file_failed",
    python_callable=lambda: 1 / 0,
    dag=dag,
    provide_context=True,
    on_failure_callback=guardar_datos_fallidos,
)

scraping_task >> branchig >> [write_file_success, write_file_failed]
