#!/bin/bash
#echo "Hola $path_script $scrapy $script $fuente $path_reports"
set -e
cd "$path_script"
$scrapy crawl $script -a url=$fuente >> $path_reports/output_$script.txt 2>&1 
echo $? > /tmp/resultado.txt
wait
