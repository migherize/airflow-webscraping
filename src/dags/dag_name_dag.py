""""
Dags para generar por plantilla Dags scrapy para Airflow.
author: Miguel Herize
mail: migherize@gmail.com
"""
import os

# import src.utils.constants as constants
# from src.utils import constants as constants
from datetime import datetime
from airflow import DAG
from airflow.operators.bash import BashOperator
import src.utils.constants as constants

default_args = {
    "start_date": datetime.now(),
}

dag = DAG("scrapy_name_dag", default_args=default_args, schedule_interval="@daily")

run_scrapy = BashOperator(
    task_id="run_scrapy",
    bash_command="call_script/plantilla_general.sh",
    params={
        "path_script": os.path.join(
            constants.AIRFLOW_HOME,
            constants.PATH_PROJECT_SCRAPY,
            "plantilla_project/tutorial",
        ),
        "scrapy": os.path.join(constants.VIRTUAL_ENV, "bin/scrapy"),
        "spider": "name_spider",
        "url": "www.fuente.com",
        "path_reports": os.path.join(constants.AIRFLOW_HOME, constants.REPORTS),
    },
    env={
        "path_script": "{{ params.path_script }}",
        "scrapy": "{{ params.scrapy }}",
        "script": "{{ params.spider }}",
        "fuente": "{{ params.url }}",
        "path_reports": "{{ params.path_reports }}",
    },
    output_encoding="utf-8",
    dag=dag,
)
