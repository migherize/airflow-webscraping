""""
constantes para todo el sistema de Airflow para configuraciones.
author: Miguel Herize
mail: migherize@gmail.com
"""
import os

# from dotenv import load_dotenv

# Cargar .env
# load_dotenv()
PATH_HOME = os.getcwd()
AIRFLOW_HOME = os.getenv("AIRFLOW_HOME")
VIRTUAL_ENV = os.getenv("VIRTUAL_ENV")
PATH_PROJECT_SCRAPY = "spiders"
PATH_YMAL = "template_yaml"
PATH_JINJA = "template_jinja"
TEMPLATE_JINJA = "template_scrapy.jinja2"
REPORTS = "reports"
SUCCESS = "exitosos.txt"
FAILED = "reports"
PATH_FOLDER_SH = "dags/call_script"
BASH_FILE = "plantilla_general.sh"
KAFKA_TOPIC = "items"
KAFKA_BOOTSTRAP_SERVERS = "localhost:9092"
