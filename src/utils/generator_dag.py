""""
Generador de Dags con plantilla Ymal.
author: Miguel Herize
mail: migherize@gmail.com
"""
import os
from argparse import ArgumentParser
import yaml
from jinja2 import Environment, FileSystemLoader
import constants


def arguments():
    """
    Recibe argumentos desde la consola

    Ejemplo: python3 generator_dag.py --input_yaml input_plantilla --template_jinja template_scrapy

    """
    stdin_parser = ArgumentParser()
    stdin_parser.add_argument(
        "--input_yaml",
        type=str,
        help="Name of the input yaml.",
    )
    stdin_parser.add_argument(
        "--template_jinja",
        type=str,
        help="Name of the template Jinja2.",
    )
    stdin_values = vars(stdin_parser.parse_args())
    return stdin_values


def dag_template(name_dag=None, template_jinja=None):
    """plantilla para generar el o los dags para los webscraping"""
    file_dir = os.path.dirname(os.path.abspath(__file__))
    env = Environment(loader=FileSystemLoader(file_dir))
    script_jinja = os.path.join(constants.PATH_JINJA, f"{template_jinja}.jinja2")
    template = env.get_template(script_jinja)
    file_dir = os.path.join(file_dir, constants.PATH_YMAL)

    # Un solo dag especifico
    if name_dag:
        path_archivo = os.path.join(file_dir, f"{name_dag}.yaml")
        if os.path.exists(path_archivo):
            with open(path_archivo, "r", encoding="utf-8") as configfile:
                config = yaml.safe_load(configfile)
                with open(
                    f"{constants.AIRFLOW_HOME}/dags/dag_{config['dag_id']}.py",
                    "w",
                    encoding="utf-8",
                ) as file:
                    file.write(template.render(config))
        else:
            raise FileNotFoundError(
                f"Archivo con el nombre: {name_dag}.ymal no encontrado"
            )
    # Todos los dags en la carpeta
    else:
        for filename in os.listdir(file_dir):
            if filename.endswith(".yaml"):
                with open(
                    f"{file_dir}/{filename}", "r", encoding="utf-8"
                ) as configfile:
                    config = yaml.safe_load(configfile)
                    with open(
                        f"{constants.AIRFLOW_HOME}/dags/dag_{config['dag_id']}.py",
                        "w",
                        encoding="utf-8",
                    ) as file:
                        file.write(template.render(config))


if __name__ == "__main__":
    arg_input = arguments()
    INPUT = arg_input["input_yaml"]
    TEMPLATE = arg_input["template_jinja"]
    bash_file_path = os.path.join(
        constants.AIRFLOW_HOME, constants.PATH_FOLDER_SH, constants.BASH_FILE
    )
    os.chmod(bash_file_path, 0o700)
    if INPUT and TEMPLATE:
        dag_template(INPUT, TEMPLATE)
    else:
        dag_template()
