# Airflow Webscraping

En este repositorio encontraras información de como instalar, usar, implementar en forma local una instancia de Airflow para ejecutar un servicio de webscrapy con python.

# Documentación

## Webscraping

Web scraping es la técnica de extracción automatizada de datos de sitios web. Permite a los usuarios extraer información de una variedad de fuentes en línea de manera rápida y eficiente.

![webscraping](/docs/img/webscraping.png)

## Airflow
Airflow es una plataforma de programación de flujo de trabajo open source que permite a los usuarios diseñar, programar y monitorear procesos de datos complejos, adicionalmente te da una interfaces gráfica para monitorear todos estos procesos. 

![Airflow](/docs/img/airflow.png)

La estructura de Airflow se compone de varios componentes que trabajan juntos para permitir la creación, planificación, monitoreo y ejecución de flujos de trabajo complejos. A continuación, se describen los componentes principales de la estructura de Airflow:

- `base de datos`: Airflow utiliza una base de datos para almacenar metadatos sobre los DAG, las tareas, las instancias de ejecución, el estado actual de las tareas y otros detalles relevantes. 

- `Webserver`: El webserver de Airflow es la interfaz de usuario principal que permite la visualización y administración de los DAG.

- `Scheduler`: El scheduler de Airflow es el motor que planifica y orquesta las ejecuciones de las tareas en función de las dependencias del DAG. 

- `Workers`: Los workers de Airflow son procesos que ejecutan las tareas individuales de un DAG.

- `Executor`: El executor de Airflow es responsable de coordinar la ejecución de las tareas en los workers.

- `DAGs`: Los DAGs son la representación de alto nivel de los flujos de trabajo en Airflow.

    ![Scheduler](/docs/img/scheduler.png)


# Ambiente

Se requiere construir un flujo de trabajo para procesar Dags para webscraping python, donde se permita crear, monitorear y ejecutar estos flujos complejos. 

![Flujo de Trabajo](/docs/img/flujo_de_trabajo.png)

Para ello este proyecto se a dividido en fases: 

- `Fase 1 Producto mínimo viable`: es el inicio de la construcción del flujo, donde se construye un prototipo para ejecutar DAGs dentro de airflow con la capacidad de realizar Webscraping con el lenguaje de programación Python.

- `Fase 2 Configuraciones de Salidas`: en esta fase se construye los diferentes tipos de salidas que puede tener el flujo de los Dags tomando en cuenta el caso ideal (success) o cualquier tipo de errores esperados (Failed).

- `Fase 3 Organizar Scheduler`: Dentro de airflow el motor que planifica y orquesta se llama Scheduler el cual se encarga de realizar configuraciones de dependencias, consultas a la Base de datos, planificación de los Dags, agendar las ejecuciones y revisar la disponibilidad de recursos para cantidad de procesos en paralelo, para ello en esta fase se trabajara dichas configuraciones.

- `Fase 4 Absorber Automatic Collection V2`: Para el flujo de trabajo de procesar Webscraping existe un proyecto que hace dicho proceso con temporizador crontab, es decir otra Tecnología ajena a Airflow, por ende el flujo de trabajo se debe acoplar a las necesidades de esta v2 y viceversa.

Nota: cada fase tendra su respectiva rama feature/fase_X

- Parametros de Entrada:

    ![Input](/docs/img/entradas.png)

- Parametros de Salida:

    ![Output](/docs/img/carpetas_salida.png)

- Componentes Principales:

    ![Componentes Principales](/docs/img/componentes_principales.png)


## Requisitos

1. Python 3.10.7
2. Pip 23.0
3. Apache-airflow 2.5.2
4. Scrapy 2.4.1
5. SQLAlchemy 2.0.4
6. IDE: Visual Studio 2022 / Visual Code / PyCharm
7. Base de datos: SQL / MySQL / Postgresql / MongoDB / CouchDB / Redis / MariaDB

## Instalación
1. Clonar el repositorio y cambiarse a la Rama
    ```
    git clone https://gitlab.com/migherize/airflow-webscraping.git
    
    cd airflow-webscraping
    git checkout dev
    ```
2. Variables de entorno para conectarse
    - `AIRFLOW_PROJ_DIR`: ruta raiz del proyecto donde esta los Dags, logs, plugins.
    - `AIRFLOW_HOME`: ruta que se utiliza para especificar la ubicación de la carpeta principal de Airflow en el sistema de archivos.
    Opcional:
    - `AIRFLOW_UID`: para especificar el ID de usuario de Airflow en el sistema en el que se está ejecutando
    - `AIRFLOW__CORE__LOAD_EXAMPLES`: se utiliza para especificar si se deben cargar o no los ejemplos de flujo de trabajo de Airflow al iniciar el servidor web de Airflow. [True or False]

    #### Crear archivo .env
    ```
    touch .env
    ```
    #### Ejemplo .env
    ```
    AIRFLOW_PROJ_DIR =path_folder_app
    AIRFLOW_HOME=path_folder_app_home
    AIRFLOW_UID= 501                        # UID de mac
    AIRFLOW__CORE__LOAD_EXAMPLES= False       # Para no cargar los ejemplos.
    ```

3. Instalar dependencias

    Para ello debemos tener instalado el sistema de gestion de paquetes pip.
    ```
    pip install --upgrade pip
    ```

    Luego con un gestor de entornos virtuales construiremos un entorno virtual para el proyecto, con el gestor de tu preferencia.
    
    * Entorno con [Pipenv](https://pypi.org/project/pipenv/):
        ```
            pip install pipenv

            cd airflow-webscraping
            
            pipenv install
            
            pipenv shell

            pipenv install -r requirements.txt
            pipenv install -r requirements-webscraping.txt
        ```

    * Entorno con [Virtualenv](https://pypi.org/project/virtualenv/):
        ```
        python -m venv airflow-webscraping

        source airflow-webscraping/bin/activate

        cd airflow-webscraping
        
        pip install -r requirements.txt
        pipenv install -r requirements-webscraping.txt
        ```

## Uso
1. Ejecutar el proyecto:

    1.1 Inicio de Servidor:
    ```
    airflow db init 
    airflow users create \
        --username admin \
        --firstname Peter \
        --lastname Parker \
        --role Admin \
        --email spiderman@superhero.org

    airflow webserver --port 8080
    ```
    1.2 Inicio de scheduler (en otro terminal con el env)
    ```
    airflow scheduler   
    ```
    1.3 Imagen de servidor y scheduler
![Server and scheduler](/docs/img/server_scheduler.png)

2. Acceder a http://localhost:8080/home


## Interface
### Home Airflow Web-scraping
![AIRFLOW](/docs/img/home.png)

## Crear un Dag

![Create Dags](/docs/img/create_dag.png)

Pasos para construir un modelo de Dags para WebScraping:

1. Construir entradas (yaml) que necesita el proyecto WebScraping.
    ```
    dag_id: 'name_dag'
    name_project: 'path_folder_name/project'
    script: 'name_spider'
    fuente: 'www.fuente.com'
    schedule_interval: '@daily'
    ```
    Nota: este Input se debe Guardar en la carpeta utils/template_yaml.

2. Construir la plantilla Jinja para crear los Dags.
```
import os
from datetime import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.operators.python import BranchPythonOperator
from airflow.models import Variable
import utils.constants as constants

dag = DAG(
    "scrapy_{{dag_id}}",
    description="Un DAG que realiza web scraping y guarda los resultados en diferentes ubicaciones",
    schedule_interval="{{schedule_interval}}",
    start_date= datetime.now(),
    catchup=False,
)

def _choose():
    resultado_file = Variable.get("resultado_file")
    with open(resultado_file, "r", encoding="utf-8") as file:
        resultado = file.read().strip()

    if resultado == "0":
        return "write_file_success"

    return "write_file_failed"

def guardar_datos_exitosos(**context):
    datos = context["ti"].xcom_pull(task_ids="run_scrapy")
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    info = f"my_spider funciono correctamente a la hora {now}"
    with open(
        os.path.join(constants.AIRFLOW_HOME, constants.REPORTS, constants.SUCCESS),
        "a",
        encoding="utf-8",
    ) as f:
        f.write(str(datos) + info + "\n")

def guardar_datos_fallidos(context):
    datos = context["exception"]
    with open(
        os.path.join(constants.AIRFLOW_HOME, constants.REPORTS,  constants.FAILED),
        "a",
        encoding="utf-8",
    ) as f:
        f.write(str(datos) + "\n")

scraping_task = BashOperator(
    task_id="run_scrapy",
    bash_command="/call_script/plantilla_general.sh",
    params={
        "path_script": os.path.join(
            constants.AIRFLOW_HOME,
            constants.PATH_PROJECT_SCRAPY,
            "{{name_project}}",
        ),
        "scrapy": os.path.join(constants.VIRTUAL_ENV, "bin/scrapy"),
        "spider": "{{script}}",
        "url": "{{fuente}}",
        "path_reports": os.path.join(constants.AIRFLOW_HOME, constants.REPORTS),
    },
    output_encoding="utf-8",
    dag=dag,
)

branchig = BranchPythonOperator(
    task_id="branchPython",
    python_callable=_choose,
)

write_file_success = PythonOperator(
    task_id="write_file_success", python_callable=guardar_datos_exitosos
)

write_file_failed = PythonOperator(
    task_id="write_file_failed",
    python_callable=lambda: 1 / 0,
    dag=dag,
    provide_context=True,
    on_failure_callback=guardar_datos_fallidos,
)

scraping_task >> branchig >> [write_file_success, write_file_failed]
```
        Nota: este Input se debe Guardar en la carpeta utils/template_jinja.

3. Crear el script de scrapy en la carpeta de los projectos_scrapy.

    1. Crear el projecto: 
    ```
    cd src/spiders/
    scrapy startproject tutorial
    cd tutorial
    ```
    2. Construir el spider dentro de el archivo spider del proyecto tutorial
    ```
    cd spiders/
    touch my_spider.py
    ```
    3. Crear la araña la cual se va a encargar de realizar el webscraping, recibiendo sus respectivos valores.
    ```

    import scrapy


    class MySpider(scrapy.Spider):
        name = "my_spider"
        allowed_domains = ["example.com"]

        def __init__(self, url=None, *args, **kwargs):
            super(MySpider, self).__init__(*args, **kwargs)
            if url:
                self.url = url
            else:
                raise ValueError("Missing required argument: url")

        def start_requests(self):
            url = self.url
            yield scrapy.Request(url=url, callback=self.parse)

        def parse(self, response):
            # Aquí implementamos la lógica de parsing
            resultado = "finalizo correctamente"
            pass

    ```
4. Ejecutar el generador_dag.py con los parámetros paso1 y paso 2.

    Para ello vamos a ir a la ruta y realizar este comando:

    ```
    cd src/utils/
    python3 generator_dag.py --input_yaml input_tutorial --template_jinja template_scrapy_fase2
    ```

5. Agregar param env para el dag.
    ```
    env={
            "path_script": "{{ params.path_script }}",
            "scrapy": "{{ params.scrapy }}",
            "script": "{{ params.spider }}",
            "fuente": "{{ params.url }}",
            "path_reports": "{{ params.path_reports }}",
        },
    ```
    Nota: este proceso se debe hacer cuando el paso 4 se realizo correctamente, ir a la carpeta src/dag/ buscar el dag creado por el paso 4 y agregar el parametro DAG debajo de params.

6. Visualizar y ejecutar Dag en la interfaces.

    Acceder a http://localhost:8080/home para visualizar su Dag con el nombre scrapy_{{dag_id}}.

## Contribución
1. Fork del repositorio
2. Crear una rama

    ```
    git checkout -b feature/nueva-funcionalidad
    ```